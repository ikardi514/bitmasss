<?php
/**
 * Created by PhpStorm.
 * User: Mahfuz
 * Date: 20-Apr-16
 * Time: 9:12 PM
 */

namespace App\bitm\user;
use pdo;

class article{
    public  $id=NULL;
    public $title=NULL;
    public $subtitle=NULL;
    public $summary=NULL;
    public $summaryWithHTML=NULL;
    public $Details=Null;
    public $DetailsWithHTML=NULL;
    public $userId=NULL;
    private $con='';

    public function __construct()
    {
        try{
            $this->con=new pdo('mysql:host=localhost;dbname=owncms','root','');
        }catch(\PDOException $e){
            echo $e;
        }

    }
    public function prepare($val){
        if(isset($val['title']) and !empty($val['title'])){
            // $this->username=$val['username'];
            $this->title=$val['title'];
        }
        if(isset($val['subtitle']) and !empty($val['subtitle'])){
            //$this->email=$val['email'];
            $this->subtitle=$val['subtitle'];
        }
        if(isset($val['summary']) and !empty($val['summary'])){
            //$this->email=$val['email'];
            $this->summary=$val['summary'];
        }
        if(isset($val['summaryWithHTML']) and !empty($val['summaryWithHTML'])){
            //$this->email=$val['email'];
            $this->summaryWithHTML=$val['summaryWithHTML'];
        }
        if(isset($val['Details']) and !empty($val['Details'])){
            //$this->email=$val['email'];
            $this->Details=$val['Details'];
        }
        if(isset($val['DetailsWithHTML']) and !empty($val['DetailsWithHTML'])){
            //$this->email=$val['email'];
            $this->DetailsWithHTML=$val['DetailsWithHTML'];
        }
        if(isset($val['userId']) and !empty($val['userId'])){
            //$this->email=$val['email'];
            $this->userId=$val['userId'];
        }
        if(isset($val['id']) and !empty($val['id'])){
            $this->id=$val['id'];
        }
    }
    public function storeArt(){
        $sql="INSERT INTO `owncms`.`articles` (`users_id`, `title`, `sub_title`, `summary`, `html_summary`, `details`, `html_details`) VALUES (:userID, :title, :subtitle, :summary, :htmlSummary, :details, :htmlDetails)";

        $res=$this->con->prepare($sql);
        $res->execute (array(':userID'=>$this->userId,':title'=>$this->title,':subtitle'=>$this->subtitle,':summary'=>$this->summary,':htmlSummary'=>$this->summaryWithHTML,':details'=>$this->Details,':htmlDetails'=>$this->DetailsWithHTML));
        if($res){
            session_start();
            $_SESSION['artstore']='Succesfully Published';
            return $this->con->lastInsertId();
        }
    }
    public function allArticle(){
        $sql="SELECT * FROM `articles` ";
        $res=  $this->con->prepare($sql);
        $res->execute();

        While($result = $res->fetch(PDO::FETCH_ASSOC)){
            $alldata[]=$result;
        }
        if(isset($alldata)){
            return $alldata;
        }
    }
    public function viewArt(){
        $sql="SELECT * FROM `articles` WHERE id=:id";
        $res=  $this->con->prepare($sql);
        $res->execute(array(':id'=> $this->id));

       $alldata = $res->fetch(PDO::FETCH_ASSOC);
            
        if(isset($alldata)){
            return $alldata;
        }
    }




    public function updateArt(){
        $sql="UPDATE `owncms`.`articles` SET `title` = :title, `sub_title` = :subtitle,`summary` = :summary, `html_summary` = :htmlSummary, `details` = :details, `html_details` = :htmlDetails WHERE `articles`.`id` = :id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':title'=>$this->title,'subtitle'=>$this->subtitle,':summary'=>$this->summary,':htmlSummary'=>$this->summaryWithHTML,':details'=>$this->Details,':htmlDetails'=>$this->DetailsWithHTML,':id'=>$this->id));
        if($res){
          // session_start();
          // $_SESSION['updateart']='<span style="color: green;">Successfully Updated</span>';
          // header('location:allarticle.php');
        }else{
            echo 'failed';
        }
    }

}