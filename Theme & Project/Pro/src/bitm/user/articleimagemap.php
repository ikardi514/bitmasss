<?php
/**
 * Created by PhpStorm.
 * User: Mahfuz
 * Date: 20-Apr-16
 * Time: 11:18 PM
 */

namespace App\bitm\user;

use pdo;
class articleimagemap{
    public $aID=NULL;
    public $iID=NULL;
    private $con='';

    public function __construct()
    {
        try{
            $this->con=new pdo('mysql:host=localhost;dbname=owncms','root','');
        }catch(\PDOException $e){
            echo $e;
        }

    }
    public function prepare($val){
        if(isset($val['aID']) and !empty($val['aID'])){
            // $this->username=$val['username'];
            $this->aID=$val['aID'];
        }
        if(isset($val['iID']) and !empty($val['iID'])){
            //$this->email=$val['email'];
            $this->iID=$val['iID'];
        }
        if(isset($val['id']) and !empty($val['id'])){
            $this->id=$val['id'];
        }
    }
    public function mapArtIMG(){
        $sql="INSERT INTO `owncms`.`articles_images_mapping` (`articles_id`, `images_id`) VALUES (:aID, :iID)";

        $res=$this->con->prepare($sql);
        $res->execute (array(':aID'=>$this->aID,':iID'=>$this->iID));
        if($res){
            if(!isset($_SESSION['artstore'])){
                $_SESSION['artstore']='Succesfully Published';
            }
        }
    }
    public function getimageIDbyArtIDfromMap($id){

        $sql="SELECT images_id FROM  `articles_images_mapping` WHERE articles_id= :id";
        //echo $sql;die();
        $res=  $this->con->prepare($sql);
        $res->execute(array(':id'=> $id));

        $alldata = $res->fetch(PDO::FETCH_ASSOC);

        if(isset($alldata)){
            return $alldata;
        }
    }
}