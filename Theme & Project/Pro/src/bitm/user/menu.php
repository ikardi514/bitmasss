<?php
/**
 * Created by PhpStorm.
 * User: Mahfuz
 * Date: 23-Apr-16
 * Time: 3:39 PM
 */

namespace App\bitm\user;

use pdo;
class menu{

    public $id='';
    public $menu=NULL;
    public $menuparent='';
    public $size=NULL;
    private $con='';


    public function __construct()
    {
        try{
            $this->con=new pdo('mysql:host=localhost;dbname=owncms','root','');
        }catch(\PDOException $e){
            echo $e;
        }

    }
    public function prepare($val){
        if(isset($val['menu']) and !empty($val['menu'])){
            // $this->username=$val['username'];
            $this->menu=$val['menu'];
        }
        if(isset($val['menuparent']) and !empty($val['menuparent'])){
            //$this->email=$val['email'];
            $this->menuparent=$val['menuparent'];
        }

        if(isset($val['id']) and !empty($val['id'])){
            $this->id=$val['id'];
        }
    }
    public function storeMenu(){
        $sql="INSERT INTO `owncms`.`menus` (`title`, `parent_id`) VALUES (:title, :parentid)";
        echo $sql;

        $res=$this->con->prepare($sql);
        $res->execute (array(':title'=>$this->menu,':parentid'=>$this->menuparent));
        if($res){
            echo 'succ';
            if(!isset($_SESSION['menustore'])){
                session_start();
                $_SESSION['menustore']='Succesfully Added';
            }
            return  $this->con->lastInsertId();

        }else{
            echo 'failed';
        }
    }
    public function updateImg(){
        $sql="UPDATE `owncms`.`images` SET `image_name` = :imageName, `extention` = :extens,`size` = :size WHERE `images`.`id` = :id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':imageName'=>$this->imageName,'extens'=>$this->extension,':size'=>$this->size,':id'=>$this->id));
        if($res){
            // session_start();
            // $_SESSION['updateart']='<span style="color: green;">Successfully Updated</span>';
            // header('location:allarticle.php');
        }else{
            echo 'failed';
        }
    }
    public function getImgNameByID(){
        $sql="SELECT image_name FROM `images` WHERE id=:id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':id'=>$this->id));
        $result = $res->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function allMenu(){
        $sql="SELECT * FROM `menus` ";
        $res=  $this->con->prepare($sql);
        $res->execute();

        While($result = $res->fetch(PDO::FETCH_ASSOC)){
            $alldata[]=$result;
        }
        if(isset($alldata)){
            return $alldata;
        }
    }
    public function MenuNameByID($id){
        $sql="SELECT title FROM `menus` WHERE parent_id=:id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':id'=>$id));
        $result = $res->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function view($id){
        $sql="SELECT title FROM `menus` WHERE id=:id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':id'=>$id));
        $result = $res->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function update(){
        $sql="UPDATE `owncms`.`menus` SET `title` = :title, `parent_id` = :parent WHERE `menus`.`id` = :id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':title'=>$this->menu,'parent'=>$this->menuparent,':id'=>$this->id));
        if($res){
           session_start();
           $_SESSION['updatemenu']='<span style="color: green;">Successfully Updated</span>';
           header('location:allmenu.php');
        }else{
            echo 'failed';
        }

    }

}