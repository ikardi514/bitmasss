<?php
/**
 * Created by PhpStorm.
 * User: Mahfuz
 * Date: 23-Apr-16
 * Time: 7:15 PM
 */

namespace App\bitm\user;

use pdo;
class articlemenumap{


    public $aID=NULL;
    public $mID=NULL;
    private $con='';

    public function __construct()
    {
        try{
            $this->con=new pdo('mysql:host=localhost;dbname=owncms','root','');
        }catch(\PDOException $e){
            echo $e;
        }

    }
    public function prepare($val){
        if(isset($val['aID']) and !empty($val['aID'])){
            // $this->username=$val['username'];
            $this->aID=$val['aID'];
        }
        if(isset($val['mID']) and !empty($val['mID'])){
            //$this->email=$val['email'];
            $this->mID=$val['mID'];
        }
        if(isset($val['id']) and !empty($val['id'])){
            $this->id=$val['id'];
        }
    }
    public function mapArtMenu(){
        $sql="INSERT INTO `owncms`.`articles_menu_mapping` (`article_id`, `menu_id`) VALUES (:aID, :mID)";

        $res=$this->con->prepare($sql);
        $res->execute (array(':aID'=>$this->aID,':mID'=>$this->mID));
        if($res){
            if(!isset($_SESSION['artstore'])){
                $_SESSION['artstore']='Succesfully Published';
            }
        }
    }
    public function getMenuIDbyArtIDfromMap($id){

        $sql="SELECT menu_id FROM `articles_menu_mapping` where article_id=:id";
        //echo $sql;die();
        $res=  $this->con->prepare($sql);
        $res->execute(array(':id'=> $id));

        $alldata = $res->fetch(PDO::FETCH_ASSOC);

        if(isset($alldata)){
            return $alldata;
        }
    }
    public function updateMapArtMenu(){
        $sql="UPDATE `owncms`.`articles_menu_mapping` SET `menu_id` = :mID WHERE `articles_menu_mapping`.`article_id` = :id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':mID'=>$this->mID,':id'=>$this->id));
        if($res){

        }else{
            echo 'failed map';
        }

    }
}