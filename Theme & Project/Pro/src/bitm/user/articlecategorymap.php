<?php
/**
 * Created by PhpStorm.
 * User: Mahfuz
 * Date: 20-Apr-16
 * Time: 11:18 PM
 */

namespace App\bitm\user;

use pdo;
class articlecategorymap{
    public $aID=NULL;
    public $cID=NULL;
    public $id=NULL;
    private $con='';

    public function __construct()
    {
        try{
            $this->con=new pdo('mysql:host=localhost;dbname=owncms','root','');
        }catch(\PDOException $e){
            echo $e;
        }

    }
    public function prepare($val){
        if(isset($val['aID']) and !empty($val['aID'])){
            // $this->username=$val['username'];
            $this->aID=$val['aID'];
        }
        if(isset($val['cID']) and !empty($val['cID'])){
            //$this->email=$val['email'];
            $this->cID=$val['cID'];
        }
        if(isset($val['id']) and !empty($val['id'])){
            $this->id=$val['id'];
        }
    }
    public function mapArtCat(){
        $sql="INSERT INTO `owncms`.`articles_categories_mapping` (`article_id`, `category_id`) VALUES (:aID, :cID)";

        $res=$this->con->prepare($sql);
        $res->execute (array(':aID'=>$this->aID,':cID'=>$this->cID));
        if($res){
            if(!isset($_SESSION['artstore'])){
                $_SESSION['artstore']='Succesfully Published';
            }
        }
    }
    public function getIdByArtID(){
        $sql="SELECT category_id FROM  `articles_categories_mapping` WHERE article_id= :id";
        $res=  $this->con->prepare($sql);
        $res->execute(array(':id'=> $this->id));

        $alldata = $res->fetch(PDO::FETCH_ASSOC);

        if(isset($alldata)){
            return $alldata;
        }

    }
    public function updateCatID(){
        $sql="UPDATE `owncms`.`articles_categories_mapping` SET `category_id` = :cID WHERE `articles_categories_mapping`.`article_id` = :aID";
        $res=$this->con->prepare($sql);
        $res->execute(array(':cID'=>$this->cID,':aID'=>$this->aID));
        if($res){

        }else{
            echo 'failed map';
        }
    }

}