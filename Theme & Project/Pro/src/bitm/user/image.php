<?php
/**
 * Created by PhpStorm.
 * User: Mahfuz
 * Date: 20-Apr-16
 * Time: 10:59 PM
 */

namespace App\bitm\user;

use pdo;
class image{

    public $id='';
    public $extension=NULL;
    public $imageName=NULL;
    public $size=NULL;
    private $con='';


    public function __construct()
    {
        try{
            $this->con=new pdo('mysql:host=localhost;dbname=owncms','root','');
        }catch(\PDOException $e){
            echo $e;
        }

    }
    public function prepare($val){
        if(isset($val['imageName']) and !empty($val['imageName'])){
            // $this->username=$val['username'];
            $this->imageName=$val['imageName'];
        }
        if(isset($val['extension']) and !empty($val['extension'])){
            //$this->email=$val['email'];
            $this->extension=$val['extension'];
        }
        if(isset($val['size']) and !empty($val['size'])){
            //$this->email=$val['email'];
            $this->size=$val['size'];
        }
        if(isset($val['id']) and !empty($val['id'])){
            $this->id=$val['id'];
        }
    }
    public function storeImg(){
        $sql="INSERT INTO `owncms`.`images` (`image_name`, `extention`, `size`) VALUES (:imgName, :extens, :size)";

        $res=$this->con->prepare($sql);
        $res->execute (array(':imgName'=>$this->imageName,':extens'=>$this->extension,':size'=>$this->size));
        if($res){
            if(!isset($_SESSION['artstore'])){
                $_SESSION['artstore']='Succesfully Added';
            }
            return  $this->con->lastInsertId();
        }
    }
    public function updateImg(){
        $sql="UPDATE `owncms`.`images` SET `image_name` = :imageName, `extention` = :extens,`size` = :size WHERE `images`.`id` = :id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':imageName'=>$this->imageName,'extens'=>$this->extension,':size'=>$this->size,':id'=>$this->id));
        if($res){
            // session_start();
            // $_SESSION['updateart']='<span style="color: green;">Successfully Updated</span>';
            // header('location:allarticle.php');
        }else{
            echo 'failed';
        }
    }
    public function getImgNameByID(){
        $sql="SELECT image_name FROM `images` WHERE id=:id";
        $res=$this->con->prepare($sql);
        $res->execute(array(':id'=>$this->id));
        $result = $res->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

}