<?php

//print_r($_POST);die();
include "../../../vendor/autoload.php";
use App\bitm\user\userreg;
use App\bitm\user\profile;
use App\bitm\user\article;
use App\bitm\user\image;
use App\bitm\user\articleimagemap;
use App\bitm\user\articlecategorymap;
use App\bitm\user\articlemenumap;

//var_dump($_POST);
$objArt=new article();
$objImg=new image();


function debug($data= "")
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

debug($_POST);
debug(($_FILES));


if(isset($_POST) && !empty($_POST)){
    $_POST['summary']=strip_tags($_POST['summaryWithHTML']);
    $_POST['Details']=strip_tags($_POST['DetailsWithHTML']);
    $objArt->prepare($_POST);
    $artID=$objArt->storeArt();
}


if(isset($_FILES) && !empty($_FILES['image']['name'])){
    $permitedImage=['jpg','jpeg','png','gif'];
    $imageName=time().$_FILES['image']['name'];
    $imageLink=$_FILES['image']['tmp_name'];
    $size=$_FILES['image']['size'];

    $filearray = explode(".",$imageName);
    $extension=strtolower(end($filearray));




    if(in_array($extension,$permitedImage)){
        $imageInfo['imageName']=$imageName;
        $imageInfo['size']=$size;
        $imageInfo['extension']=$extension;
        $objImg->prepare($imageInfo);
        $imgID=$objImg->storeImg();
        move_uploaded_file($imageLink,'../../../img/articleIMG/'.$imageName);
        //echo $imgID.'succ';


    }
    else{
        echo 'Invalid Image Format';
    }
}
if(isset($_POST['Category'])){
   $map['cID']=$_POST['Category'];
   $map['aID']=$artID;
   $categroyArtMapOBJ=new articlecategorymap();
   $categroyArtMapOBJ->prepare($map);
   $categroyArtMapOBJ->mapArtCat();
   
}


if(isset($artID) && isset($imgID)){
    $map['iID']=$imgID;
    $map['aID']=$artID;
    $artImgMapOBJ=new articleimagemap();
    $artImgMapOBJ->prepare($map);
    $artImgMapOBJ->mapArtIMG();

}

if(isset($_POST['menu']) && !empty($_POST['menu'])){
    $mapM['mID']=$_POST['menu'];
    $mapM['aID']=$artID;
    $ObjArtMenuMap=new articlemenumap();
    $ObjArtMenuMap->prepare($mapM);
    $ObjArtMenuMap->mapArtMenu();

}


//header('location:articles.php');