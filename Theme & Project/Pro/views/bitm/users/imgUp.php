<?php
include "../../../vendor/autoload.php";
use App\bitm\user\userreg;
use App\bitm\user\profile;

session_start();
$obj=new profile();

if(isset($_FILES['image'])){
    $permitedImage=['jpg','jpeg','png','gif'];
    $imageName=time().$_FILES['image']['name'];
    $imageLink=$_FILES['image']['tmp_name'];
    $imageSize=$_FILES['image']['size'];


    $imageType=strtolower(end(explode(".",$imageName)));

    $_POST['imageName']=$imageName;
    $_POST['id']=$_SESSION['useridlogin'];


    if(in_array($imageType,$permitedImage)){
         $obj->prepare($_POST);
        move_uploaded_file($imageLink,'../../../img/'.$imageName);
        unlink('../../../img/'.$_POST['oldImg']);
         $obj->profilePicUp();

    }
    else{
        echo 'Invalid Image Format';
    }
}


