<?php
session_start();
if(!isset($_SESSION['useridlogin'])){
    header('location:login.php');
}

include "../../../vendor/autoload.php";
use App\bitm\user\userreg;
use App\bitm\user\profile;

$profileObj=new profile();
$val['id']=$_SESSION['useridlogin'];
$profileObj->prepare($val);

$alldata=$profileObj->view();

//print_r($alldata); die();


$val['id']=$_SESSION['useridlogin'];
$profileObj->prepare($val);

$alldata=$profileObj->view();
$objusr=new userreg();
$objusr->prepare($val);
$userinfo=$objusr->userinfo();



?>
<html>

<head>
    <title>User|Profile</title>
    <link href="../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../Assets/css/sb-admin.css" rel="stylesheet">
    <link href="../../../Assets/css/plugins/morris.css" rel="stylesheet">
    <link href="../../../Assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <script src="../../../Assets/js/jquery.js"></script>
    <script src="../../../Assets/js/bootstrap.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/raphael.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/morris.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/morris-data.js"></script>



    <script src="../../../Assets/js/jquery.js"></script>
    <style>
        .profile {
            background-color: white;
            border-color:ghostwhite;
            border-style: solid;
            width:100%;
            height:30px;
            margin: 5px;
        }
        #profileIMG{
            height: 198px;
            max-width: 300px;
            min-width: 120px;

        }
        #uploadphotosms{
            z-index: 999;
            padding:  5px;
            position: relative;
            margin-top:-22px;
            background-color: rgba(34,34,34,0.75);
            color: white;
            width: 100px;


        }
        #uploadphotosms:hover{
            background-color: black;
        }
        .btn{
            float: right;
            width: 50%;
        }
    </style>


</head>


<body>

<div id="wrapper">

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu message-dropdown">
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>Admin</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith KK</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith HH</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-footer">
                        <a href="#">Read All New Messages</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown">
                    <li>
                        <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">View All</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $userinfo['username'];?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <ul class="nav navbar-nav side-nav">
            <li class="active" >
                <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <li >
                <a href="category.php"><i class="fa fa-fw fa-bar-chart-o"></i> Category</a>
            </li>
            <li >
                <a href="allcategory.php"><i class="fa fa-fw fa-bar-chart-o"></i>All Category</a>
            </li>
            <li>
                <a href="articles.php"><i class="fa fa-fw fa-table"></i> Articles </a>
            </li>
            <li >
                <a href="allarticle.php"><i class="fa fa-fw fa-table"></i>All Articles </a>
            </li>

            <li>
                <a href="menu.php"><i class="fa fa-fw fa-file"></i> Menu</a>
            </li>
            <li  >
                <a href="allusers.php"><i class="fa fa-fw fa-table"></i> Users </a>
            </li>

        </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>




    <div class="col-md-12">
        <div class="col-md-3"></div>
        <div class="col-md-7">
            <center>

                <?php
                if(isset($_SESSION['updateProfile'])){?>
                    <div class="alert" role="alert">
                        <strong><?php echo $_SESSION['updateProfile']; ?>!</strong>.
                    </div>

                    <?php
                    unset($_SESSION['updateProfile']);
                }
                ?>

            </center>





           <center> <h2>Your Profile Information</h2></center>

            <form action="imgUp.php" method="post" enctype="multipart/form-data">
                <center>
                <div id="profilePicPreview">
                    <?php
                    if(isset($alldata['profile_pic']) && !empty($alldata['profile_pic'])) {
                        echo '<img id="profileIMG" src="../../../img/' . $alldata['profile_pic'] . '" />';
                    }else{
                        ?>

                        <img id="profileIMG"  src="../../../img/upload.jpg">
                    <?php } ?>

                </div>

                  <p id="uploadphotosms">Change Photo</p>


                <input type="file" style="display: none;" id="profilePicUp" name="image">
                <div id="submitPic"></div>
                <input type="text" hidden name="oldImg" value="<?php echo $alldata['profile_pic']; ?>"/>


            </form>

            </center>
            <form action="profileUpdate.php" method="post">
                <section>
                    <div class="col-md-12">
                        <div class="col-md-3"><label>First Name:</label></div>
                        <div class="col-md-9"> <input type="text" class="profile profilevalue" id="firstName" name="first_name" value="<?php if(isset($alldata['first_name'])){ echo $alldata['first_name']; }?>" disabled/></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-9" id="fnm"></div>

                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label>Last Name</label></div>
                        <div class="col-md-9"><input type="text" class="profile profilevalue" id="lastName" name="last_name" value="<?php if(isset($alldata['last_name'])){ echo $alldata['last_name']; }?>" disabled/></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-9"  id="lnm"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label>Personal Phone</label></div>
                        <div class="col-md-9"><input type="text" class="profile profilevalue" id="personalPhone" name="personal_phone" value="<?php if(isset($alldata['personal_phone'])){ echo $alldata['personal_phone']; }?>" disabled/></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-9"  id="ppm"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label>Home Phone</label></div>
                        <div class="col-md-9"><input type="text" class="profile profilevalue" id="homePhone" name="home_phone" value="<?php if(isset($alldata['home_phone'])){ echo $alldata['home_phone']; }?>" disabled/></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-9"  id="hpm"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label>Office Phone</label></div>
                        <div class="col-md-9"><input type="text" class="profile profilevalue" id="officePhone" name="office_phone" value="<?php if(isset($alldata['office_phone'])){ echo $alldata['office_phone']; }?>" disabled/></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-9"  id="opm"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label>Current Address</label></div>
                        <div class="col-md-9"><input type="text" class="profile profilevalue" id="currentAdd" name="current_address" disabled value="<?php if(isset($alldata['current_address'])){ echo $alldata['current_address']; }?>"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-9" id="cam"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label>Permanent Address</label></div>
                        <div class="col-md-9"><input type="text" class="profile profilevalue" id="permanentAdd" name="permanent_address" disabled value="<?php if(isset($alldata['permanent_address'])){ echo $alldata['permanent_address']; }?>"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-9"  id="pam"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">

                            <input type="button" name="btn" class="btn" id="btnEditProfile"  value="Edit">
                            <input type="submit" name="btn" class="btn " id="btnUpdateProfile" style="display: none;" value="Update">
                        </div>
                        <br/><br/><br/><br/><br/><br/>

                    </div>
                </section>
            </form>

        </div>
        <div class="col-md-2"></div>

    </div>


</div>






<script>
    $(document).ready(function(){
        $('#btnEditProfile').click(function(){
            $(this).hide();
            $('#btnUpdateProfile').show();
            $('.profilevalue').removeAttr('disabled');
            $('.profilevalue').css('border-color','darkgray');

        });
        $('#uploadphotosms').click(function(){
            $('#profilePicUp').trigger("click");
        });

        $("#profilePicUp").change(function(e){
            var profilePicURL =URL.createObjectURL(e.target.files[0]);
            $("#profilePicPreview").empty().append('<img id="profileIMG" src="' + profilePicURL +'" />');
            $("#submitPic").empty().append('<br/><input type="submit" id="profileImgUp" name="btn" value="SAVE"/>');

            $('#uploadphotosms').click(function(){
                $('#profilePicUp').trigger("click");
            });

        });

    })
    $(document).ready(function(){
        var fn= 0,ln= 0,pp= 0,hp= 0,op= 0,ca= 0,pa=0;
        $('#firstName').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            if(ul>2 && ul<20){

                $('#fnm').empty();
                fn=0;


            }else{
                $('#fnm').empty().append('<span style="color:red">At least 3 character and not more then 20 character</span>');
                fn=1;

            }
        })
        $('#lastName').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            if(ul>2 && ul<20){

                $('#lnm').empty();
                ln=0;


            }else{
                $('#lnm').empty().append('<span style="color:red">At least 3 character and not more then 20 character</span>');
                ln=1;

            }
        })
        $('#homePhone').focusout(function(){

            var val=$(this).val();
            var ul=val.length;
            if(ul<=11 && ul>3 && $.isNumeric(val) || ul==0){

                $('#hpm').empty();
                hp=0;


            }else{
                $('#hpm').empty().append('<span style="color:red">4-11 Numeric Number</span>');
                hp=1;

            }
        })
        $('#officePhone').focusout(function(){

            var val=$(this).val();
            var ul=val.length;
            if(ul<=11 && ul>3 && $.isNumeric(val) || ul==0){

                $('#opm').empty();
                op=0;


            }else{
                $('#opm').empty().append('<span style="color:red">4-11 Numeric Number</span>');
                op=1;

            }
        })
        $('#personalPhone').focusout(function(){

            var val=$(this).val();
            var ul=val.length;
            if(ul==11 && $.isNumeric(val) || ul==0){

                $('#ppm').empty();
                pp=0;


            }else{
                $('#ppm').empty().append('<span style="color:red">11 Numeric Number</span>');
                pp=1;

            }
        })
        $('#currentAdd').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            if(ul>6 && ul<30){

                $('#cam').empty();
                ca=0;


            }else{
                $('#cam').empty().append('<span style="color:red">6-30 Characters<span>');
                ca=1;

            }
        })
        $('#permanentAdd').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            if(ul>6 && ul<30){

                $('#pam').empty();
                pa=0;


            }else{
                $('#pam').empty().append('<span style="color:red">6-30 Characters</span>');
                pa=1;

            }
        })
        $('#btnUpdateProfile').click(function(e){

            if(fn==1 || ln==1 || pp==1 || hp==1 || op==1 || ca==1 || pa==1){
                e.preventDefault();
            }
        })
    })
</script>
</body>
</html>