<?php
session_start();
?>





<!DOCTYPE html>
<html lang="en">

<head>
    <script src="../../../Assets/js/jquery.js"></script>
    <link href="../../../Assets/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<br/><br/><br/><br/>
<div class="col-md-12">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?php
        if(isset($_SESSION['registered'])){?>
            <div class="alert alert-success" role="alert">
                <strong><?php echo $_SESSION['registered']; ?>!</strong>.
            </div>

            <?php
            unset($_SESSION['registered']);
        }
        ?>

        <legend ><h1>Registration</h1></legend>
        <form action="store.php" role="form" method="post">

            <div class="form-group">
                <label>User Name</label>
                <input type="text" class="form-control" id="userName" required name="username"/>
                <span id="user"></span>
            </div>

            <div class="form-group">
                <label>Your Email</label>
                <input type="email" id="mail" class="form-control"required name="email"/>
                <span id="mailal"></span>

            </div>
            <div class="form-group">
                <label>Your Password</label>
                <input type="password" id="password" class="form-control"required name="password"/>
                <span id="passal"></span>
            </div>
            <input type="submit" name="btn" id="submit"  class="btn btn-primary" value="Register Now"/>
            <a href="login.php" class="alert-link">Login here</a>

        </form>

    </div>
    <div class="col-md-4"></div>

</div>

<script>

    $(document).ready(function(){
        var pe= 0,ne= 0,em=0;
        $('#userName').focusout(function(){

            var userName=$(this).val();
            var ul=userName.length;
            var whitespace1st=userName.split(/[^ \t\r\n]/)[0].length;
            if(ul>2 && ul<15) {

                $('#user').empty();
                ne = 0;
                var space = /^(\S+)\s(.*)/;

                if (space.test(userName) || whitespace1st>0) {
                    $('#user').empty().append('<span style="color:red">Whitespace not allowed</span>');
                    ne = 1;
                } else {

                }
            }else{
                $('#user').empty().append('<span style="color:red">At least 3 character and not more then 15 character</span>');
                ne = 1;
            }
        })
        $('#mail').focusout(function(){

            var mail=$(this).val();
            var emailReg = /\S+@\S+\.\S+/ ;
            if(emailReg.test(mail)){

                $('#mailal').empty();
                em=0;

            }else{
                $('#mailal').empty().append('<span style="color:red">Enter Valid Mail</span>');
                em=1;
            }
        })
        $('#password').focusout(function(){

            var pass=$(this).val();
            var passVal =(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{4,12}$/);
            if(passVal.test(pass)){

                $('#passal').empty();
                pe=0;

            }else{
                $('#passal').empty().append('<span style="color:red">Combination of uppercase,Lowercase and numeric value and contain 4-12 character<span>');
                pe=1;

            }
        })
        $('#submit').click(function(e){

            if(ne==1 || em==1 || pe==1){
                e.preventDefault();
            }
        })
    })
</script>
</body>

</html>
