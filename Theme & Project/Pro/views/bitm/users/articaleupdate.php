<?php

//print_r($_POST);die();
include "../../../vendor/autoload.php";

use App\bitm\user\article;
use App\bitm\user\category;
use App\bitm\user\image;
use App\bitm\user\articleimagemap;
use App\bitm\user\articlecategorymap;
use App\bitm\user\articlemenumap;

//var_dump($_POST);
$objArt=new article();
$objImg=new image();
$objCatArtMap=new articlecategorymap();
$objArtImgMap=new articleimagemap();





if(isset($_POST) && !empty($_POST)){
    $_POST['summary']=strip_tags($_POST['summaryWithHTML']);
    $_POST['Details']=strip_tags($_POST['DetailsWithHTML']);
    $objArt->prepare($_POST);
    $artID=$objArt->updateArt();

    $CatID['cID']=$_POST['Category'];
    $CatID['aID']=$_POST['id'];
    $objCatArtMap->prepare($CatID);
    $objCatArtMap->updateCatID();
}
if(isset($_FILES) && !empty($_FILES['image']['name'])){

    $imgID=$objArtImgMap->getimageIDbyArtIDfromMap($_POST['id']);
    $imgID['id']=$imgID['images_id'];
    $objImg->prepare($imgID);
    $oldImageName=$objImg->getImgNameByID();
    //print_r($oldImageName);die();

    $permitedImage=['jpg','jpeg','png','gif'];
    $imageName=time().$_FILES['image']['name'];
    $imageLink=$_FILES['image']['tmp_name'];
    $size=$_FILES['image']['size'];

    $extension=strtolower(end(explode(".",$imageName)));

    if(in_array($extension,$permitedImage)){

        $imageInfo['imageName']=$imageName;
        $imageInfo['size']=$size;
        $imageInfo['extension']=$extension;
        $imageInfo['id']=$imgID['images_id'];
        $objImg->prepare($imageInfo);
        $imgID=$objImg->updateImg();


        unlink('../../../img/articleIMG/'.$oldImageName['image_name']);
        move_uploaded_file($imageLink,'../../../img/articleIMG/'.$imageName);
        //echo $imgID.'succ';


    }
    else{
        echo 'Invalid Image Format';
    }
}
if(isset($_POST['menu']) && !empty($_POST['menu'])){
    $mapM['mID']=$_POST['menu'];
    $mapM['id']=$_POST['id'];
    $ObjArtMenuMap=new articlemenumap();
    $ObjArtMenuMap->prepare($mapM);
    $ObjArtMenuMap->updateMapArtMenu();

}

session_start();
$_SESSION['artstore']='<span style="color:green;">Successfully Updated</span>';
header('location:allarticle.php');
