<?php
session_start();
if(!isset($_SESSION['useridlogin'])){
    header('location:login.php');
}

include "../../../vendor/autoload.php";
use App\bitm\user\userreg;
use App\bitm\user\profile;
use App\bitm\user\category;

$profileObj=new profile();
$val['id']=$_SESSION['useridlogin'];
$profileObj->prepare($val);

$alldata=$profileObj->view();
$objusr=new userreg();
$objusr->prepare($val);
$userinfo=$objusr->userinfo();
//print_r($userinfo);

$objCat=new category();
$allCategory=$objCat->allCategory();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../Assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../Assets/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../../Assets/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../Assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



</head>
<body>



<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Admin</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu message-dropdown">
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>Admin</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith KK</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith HH</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-footer">
                    <a href="#">Read All New Messages</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">View All</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $userinfo['username'];?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li >
                <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <li class="active">
                <a href="category.php"><i class="fa fa-fw fa-bar-chart-o"></i> Category</a>
            </li>
            <li >
                <a href="allcategory.php"><i class="fa fa-fw fa-bar-chart-o"></i>All Category</a>
            </li>
            <li>
                <a href="articles.php"><i class="fa fa-fw fa-table"></i> Articles </a>
            </li>
            <li >
                <a href="allarticle.php"><i class="fa fa-fw fa-table"></i>All Articles </a>
            </li>

            <li>
                <a href="menu.php"><i class="fa fa-fw fa-file"></i> Menu</a>
            </li>
            <li  >
                <a href="allusers.php"><i class="fa fa-fw fa-table"></i> Users </a>
            </li>

        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>







<br/> <br/> <br/>
<div class="col-md-12">
    <div class="col-md-4"> </div>
    <div class="col-md-4">
        <?php
        if(isset($_SESSION['catAdd'])){?>
            <div class="alert alert-success" role="alert">
                <strong><?php echo $_SESSION['catAdd']; ?>!</strong>.
            </div>

            <?php
            unset($_SESSION['catAdd']);
        }
        ?>


        <legend ><h1>Add Category</h1></legend>
        <form action="addcategory.php" role="form" method="post">

            <div class="form-group">
                <label>Category Name</label>

                <input type="text" class="form-control" required name="category"/>
            </div>


            <div class="form-group">
                <label>Parent Category</label>
                <select name="CategoryParent"  class="form-control">
                    <option value=""> Select Parent</option>
                    <?php if(isset($allCategory)){
                        foreach($allCategory as $par){
                    ?>
                    <option value="<?php echo $par['id'];?>"> <?php echo $par['title'];?></option>
                    <?php }}?>
                </select>



            </div>

            <input type="submit" name="btn"  class="btn btn-primary" value="Add Now"/>
            <a href="allcategory.php">All Category</a>


        </form>

    </div>
</div>















<!-- jQuery -->
<script src="../../../Assets/js/jquery.js"></script>
<script src="../../../Assets/js/bootstrap.min.js"></script>
<script src="../../../Assets/js/plugins/morris/raphael.min.js"></script>
<script src="../../../Assets/js/plugins/morris/morris.min.js"></script>
<script src="../../../Assets/js/plugins/morris/morris-data.js"></script>

</body>


</html>
