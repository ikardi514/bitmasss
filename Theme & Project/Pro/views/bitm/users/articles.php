<?php

session_start();
if(!isset($_SESSION['useridlogin'])){
    header('location:login.php');
}

include "../../../vendor/autoload.php";
use App\bitm\user\userreg;
use App\bitm\user\profile;
use App\bitm\user\category;
use App\bitm\user\menu;

$profileObj=new profile();
$val['id']=$_SESSION['useridlogin'];
$profileObj->prepare($val);

$alldata=$profileObj->view();
$objusr=new userreg();
$objusr->prepare($val);
$userinfo=$objusr->userinfo();



$objCat=new category();
$allCategory=$objCat->allCategory();
?>


<!DOCTYPE html>
<html lang="en">

<head>


    <script src="../../../Assets/tinymce/js/tinymce/tinymce.min.js"></script>
    <link href="../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../Assets/css/sb-admin.css" rel="stylesheet">
    <link href="../../../Assets/css/plugins/morris.css" rel="stylesheet">
    <link href="../../../Assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <script src="../../../Assets/js/jquery.js"></script>
    <script src="../../../Assets/js/bootstrap.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/raphael.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/morris.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/morris-data.js"></script>

    <meta charset="utf-8">
    <title>Article Add here</title>
    <!-- Make sure the path to CKEditor is correct. -->
    <script src="../../../Assets/ckeditor/ckeditor.js"></script>










    <title>Article|</title>

</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu message-dropdown">
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>Admin</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith KK</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith HH</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-footer">
                        <a href="#">Read All New Messages</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown">
                    <li>
                        <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">View All</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $userinfo['username'];?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li >
                    <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li >
                    <a href="category.php"><i class="fa fa-fw fa-bar-chart-o"></i> Category</a>
                </li>
                <li >
                    <a href="allcategory.php"><i class="fa fa-fw fa-bar-chart-o"></i>All Category</a>
                </li>
                <li class="active">
                    <a href="articles.php"><i class="fa fa-fw fa-table"></i> Articles </a>
                </li>
                <li>
                    <a href="allarticle.php"><i class="fa fa-fw fa-table"></i>All Articles </a>
                </li>

                <li>
                    <a href="menu.php"><i class="fa fa-fw fa-file"></i> Menu</a>
                </li>
                <li  >
                    <a href="allusers.php"><i class="fa fa-fw fa-table"></i> Users </a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>


<br/><br/><br/>



    <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-7">
            <h1>Publish Your Articles Here</h1>
            <?php
            if(isset($_SESSION['artstore'])){?>
                <div class="alert alert-success" role="alert">
                    <strong><?php echo $_SESSION['artstore']; ?>!</strong>.
                </div>

                <?php
                unset($_SESSION['artstore']);
            }
            ?>
        </div>
        <div class="col-md-3"></div>
        </div>
<div class="col-md-12"></div>
        <div class="col-md-4">
            <form action="articlestore.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Sub Title</label>
                    <input type="text" name="subtitle" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="Category"  class="form-control">
                        <option value=""> Select Category</option>
                        <?php if(isset($allCategory)){
                            foreach($allCategory as $par){
                                ?>
                                <option value="<?php echo $par['id'];?>"> <?php echo $par['title'];?></option>
                            <?php }}?>
                    </select>
                </div>
                </div>
        <div class="col-md-5">

            <div class="form-group">
            <label>Your Article Summay</label>
            <textarea name="summaryWithHTML" id="editor1" rows="10" cols="80"></textarea>
            </div>

                <script>
                    CKEDITOR.replace( 'editor1' );
                </script>
                </div>

    <div class="col-md-3" style="border: 1px solid black;width: 200px; margin-top: 24px">
        <h3>Select Menu</h3><br/>

        <?php
        $objMenu=new menu();
        $allMenu=$objMenu->allMenu();
        foreach ($allMenu as $menu){
            echo '<input type="radio" name="menu" value="'.$menu['id'].'"/>'.$menu['title'].'</br>';
        }

        ?>
        <br/><br/><br/>
    </div>

        <div class="col-md-9">
            
                <div class="form-group">
                <label>Your Article Details</label>
                <textarea name="DetailsWithHTML" id="editor2" rows="10" cols="80"></textarea>
                </div>

                <script>
                    CKEDITOR.replace( 'editor2' );
                </script>
                <div class="form-group">
                    <label>Add Image</label>
                    <input type="file" name="image" class="form-control" />
                </div>
                <input type="hidden" name="userId" value="<?php echo $_SESSION['useridlogin'];?>"/>
                <input type="submit" value="Article Publish" class="btn btn-primary navbar-btn"/>
            </form>

        </div>
        <div class="col-md-3"></div>
        
    </div>



</div>
</body>

</html>
