<?php
session_start();
if(!isset($_SESSION['useridlogin'])){
    header('location:login.php');
}
include "../../../vendor/autoload.php";
use App\bitm\user\userreg;
use App\bitm\user\profile;
use App\bitm\user\article;
use App\bitm\user\menu;


$profileObj=new profile();
$val['id']=$_SESSION['useridlogin'];
$profileObj->prepare($val);

$alldata=$profileObj->view();
$objusr=new userreg();
$objusr->prepare($val);
$userinfo=$objusr->userinfo();



$artObj=new menu();
$allArticle=  $artObj->allMenu();
//print_r($allArticle);die();
//print_r($allNormalUser);die();

//print_r($artObj->MenuNameByID(8));die();



?>
<html>
<head>
    <title>Article</title>
    <link href="../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../Assets/css/sb-admin.css" rel="stylesheet">
    <link href="../../../Assets/css/plugins/morris.css" rel="stylesheet">
    <link href="../../../Assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <script src="../../../Assets/js/jquery.js"></script>
    <script src="../../../Assets/js/bootstrap.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/raphael.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/morris.min.js"></script>
    <script src="../../../Assets/js/plugins/morris/morris-data.js"></script>



    <script src="../../../Assets/js/jquery.js"></script>

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Admin</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu message-dropdown">
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>Admin</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith KK</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith HH</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-footer">
                    <a href="#">Read All New Messages</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">View All</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $userinfo['username'];?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li >
                <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <li >
                <a href="category.php"><i class="fa fa-fw fa-bar-chart-o"></i> Category</a>
            </li>
            <li >
                <a href="allcategory.php"><i class="fa fa-fw fa-bar-chart-o"></i>All Category</a>
            </li>
            <li>
                <a href="articles.php"><i class="fa fa-fw fa-table"></i> Articles </a>
            </li>
            <li class="active">
                <a href="allarticle.php"><i class="fa fa-fw fa-table"></i>All Articles </a>
            </li>

            <li>
                <a href="menu.php"><i class="fa fa-fw fa-file"></i> Menu</a>
            </li>
            <li  >
                <a href="allusers.php"><i class="fa fa-fw fa-table"></i> Users </a>
            </li>

        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>












<div class="col-md-12">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <center>
            <br/><br/><br/><br/>
            <a href="logout.php">
                <button class="btn btn-default" type="button">
                    <em class="glyphicon glyphicon-log-out"></em> Logout
                </button>
            </a>
            <a href="profile.php">
                <button class="btn btn-default" type="button">
                    <em class="glyphicon glyphicon-certificate"></em> Your Profile
                </button>
            </a>
            <a href="logindone.php">
                <button class="btn btn-default" type="button">
                    <em class="glyphicon glyphicon-home"></em> Home
                </button>
            </a>
            <br/><br/><br/><br/>
            <?php if(isset($_SESSION['updateart'])){
                echo $_SESSION['updateart'];
                unset($_SESSION['updateart']);
            }?>

            <br/><br/>
            <?php if(isset($_SESSION['updatemenu'])){
                echo $_SESSION['updatemenu'];
                unset($_SESSION['updatemenu']);
            }?>

            <table class="table table-bordered table-hover table-responsive">
                <tr bgcolor="#1e90ff">
                    <td>Menu</td>
                    <td>Parent Menu</td>
                    <td>Action/Active</td>
                </tr>
                <?php foreach($allArticle as $data){?>
                    <tr>
                        <td><?php echo $data['title'];?></td>
                        <td><?php
                            //var_dump($data['parent_id']);
                            if(isset($data['parent_id']) && !empty($data['parent_id'])){

                                $id=$data['parent_id'];

                               $pname=$artObj->view($id);
                               echo $pname['title'];
                            }
                            ?></td>
                        <td>
                            <a href="menuedit.php?id=<?php echo $data['id'];?> && pID=<?php echo $data['parent_id'];?>"> Edit </a>|
                            <a href="makemenu.php?id=<?php echo $data['id'];?>"> Make Menu</a>
                        </td>
                    </tr>
                <?php }?>

            </table>
        </center>
    </div>
    <div class="col-md-3"></div>

</div>
</body>
</html>

