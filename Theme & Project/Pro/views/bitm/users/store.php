<?php
session_start();
include "../../../vendor/autoload.php";
use App\bitm\user\userreg;
use App\bitm\user\profile;
//var_dump($_POST);
$obj=new userreg();

$_POST['unique_id']= uniqid();

//print_r($_POST);
$obj->prepare($_POST);
$chk = $obj->checkUsername();
if ($chk != 1) {

    $obj->store();
    $id = $obj->getIdByUniqueID();
    $objProf = new profile();
    if (isset($id['id'])) {
        $objProf->storeId($id['id']);
    }
}

else {
    $_SESSION['registered']='Username not available';
    header('location:usrcreate.php');
}