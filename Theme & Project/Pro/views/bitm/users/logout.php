<?php

session_start();
if(isset($_SESSION['useridlogin'])){
    unset($_SESSION['useridlogin']);
}
if(isset($_SESSION['is_admin'])){
    unset($_SESSION['is_admin']);
}

header('location:login.php');
