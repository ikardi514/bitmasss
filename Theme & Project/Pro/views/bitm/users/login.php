<?php
//include_once 'design.php';
session_start();

if(isset($_SESSION['useridlogin'])){
    if(isset($_SESSION['is_admin'])){
        header('location:index.php');
    }else{
        header('location:login.php');
    }



}
?>
<html>
<head>
    <script src="../../../Assets/js/jquery.js"></script>
    <link href="../../../Assets/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<br/> <br/> <br/>
<div class="col-md-12">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?php
        if(isset($_SESSION['failed'])){?>
            <div class="alert alert-success" role="alert">
                <strong><?php echo $_SESSION['failed']; ?>!</strong>.
            </div>

            <?php
            unset($_SESSION['failed']);
        }
        ?>


        <legend ><h1>Login</h1></legend>
        <form action="logindone.php" role="form" method="post">

            <div class="form-group">
                <label>User Name</label>
                <input type="text" class="form-control" required name="username"/>
            </div>


            <div class="form-group">
                <label>Your Password</label>
                <input type="password" class="form-control"required name="password"/>
            </div>

            <input type="submit" name="btn"  class="btn btn-primary" value="Login"/>
            <a href="usrcreate.php" class="alert-link">For Registration</a>

        </form>

    </div>


</div>




<?php
//print_r($_SERVER);
//echo $_SERVER['SERVER_NAME'];
//
//
//$root = $_SERVER["DOCUMENT_ROOT"];
//echo $root;
?>






</body>
</html>
