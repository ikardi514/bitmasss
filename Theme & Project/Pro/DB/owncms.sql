-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2016 at 05:02 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `owncms`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
`id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `html_summary` varchar(255) NOT NULL,
  `details` varchar(512) NOT NULL,
  `html_details` varchar(1024) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `users_id`, `title`, `sub_title`, `summary`, `html_summary`, `details`, `html_details`, `created_at`, `modified_at`, `deleted_at`) VALUES
(10, 12, 'Simulation', 'Introduction to Simulation', 'Simulation enables the study of, and experimentation with, the internal interactions of a complex system, or of a subsystem within a complex system.\r\n', '<p><code>Simulation enables the study of, and experimentation with, the internal interactions of a complex system, or of a subsystem within a complex system.</code></p>\r\n', 'Simulation enables the study of, and experimentation with, the internal interactions of a complex system, or of a subsystem within a complex system.Informational, organizational, and environmental changes can be simulated, and the effect of these alterations on the model&rsquo;s behavior can be observed.The knowledge gained in designing a simulation model may be of great value toward suggesting improvement in the system under investigation.By changing simulation inputs and observing the resulting outputs, v', '<p>Simulation enables the study of, and experimentation with, the internal interactions of a complex system, or of a subsystem within a complex system.Informational, organizational, and environmental changes can be simulated, and the effect of these alterations on the model&rsquo;s behavior can be observed.The knowledge gained in designing a simulation model may be of great value toward suggesting improvement in the system under investigation.By changing simulation inputs and observing the resulting outputs, valuable insight may be obtained into which variables are most important and how variables interact.Simulation can be used as a pe</p>\r\n', '2016-04-23 19:48:02', '2016-04-23 20:10:58', NULL),
(11, 12, 'Green Abstrac', 'Abstrac', 'requires special training. It is an art that is learned over time and through experience. Furthermore, if two models are constructed by two competent individuals, they may have similarities, but it is highly unlik\r\n', '<h2 style="font-style:italic">requires special training. It is an art that is learned over time and through experience. Furthermore, if two models are constructed by two competent individuals, they may have similarities, but it is highly unlik</h2>\r\n', 'ay be difficult to interpret. Since most simulation outputs are essentially random variables (they are usually based on random inputs), it may be hard to determine whether an observation is a result of system interrelationships may be difficult to interpret. Since most simulation outputs are essentially random variables (they are usually based on random inputs), it may be hard to determine whether an observation is a result of system interrelationships.\r\n', '<p>ay be difficult to interpret. Since most simulation outputs are essentially random variables (they are usually based on random inputs), it may be hard to determine whether an observation is a result of system interrelationships may be difficult to interpret. Since most simulation outputs are essentially random variables (they are usually based on random inputs), it may be hard to determine whether an observation is a result of system interrelationships.</p>\r\n', '2016-04-23 20:11:59', '2016-04-23 20:23:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles_categories_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_categories_mapping` (
`id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles_categories_mapping`
--

INSERT INTO `articles_categories_mapping` (`id`, `article_id`, `category_id`, `created_at`, `modified_at`, `deleted_at`) VALUES
(7, 10, 10, '2016-04-23 19:48:02', '0000-00-00 00:00:00', NULL),
(8, 11, 3, '2016-04-23 20:12:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles_images_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_images_mapping` (
`id` int(11) NOT NULL,
  `articles_id` int(11) NOT NULL,
  `images_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles_images_mapping`
--

INSERT INTO `articles_images_mapping` (`id`, `articles_id`, `images_id`, `created_at`, `modified_at`, `deleted_at`) VALUES
(6, 10, 14, '2016-04-23 19:48:02', '0000-00-00 00:00:00', NULL),
(7, 11, 15, '2016-04-23 20:12:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles_menu_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_menu_mapping` (
`id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles_menu_mapping`
--

INSERT INTO `articles_menu_mapping` (`id`, `article_id`, `menu_id`, `created_at`, `modified_at`, `deleted_at`) VALUES
(3, 11, 8, '2016-04-23 19:48:02', '2016-04-23 20:43:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `left` varchar(255) NOT NULL,
  `right` varchar(255) NOT NULL,
  `parent_id` varchar(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `left`, `right`, `parent_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'News', '', '', '', '0000-00-00 00:00:00', '2016-04-23 14:03:38', '0000-00-00 00:00:00'),
(3, 'Entertaiment', '', '', '1', '0000-00-00 00:00:00', '2016-04-23 15:38:14', '0000-00-00 00:00:00'),
(4, 'Sports', '', '', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Cricket', '', '', '4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Entertain', '', '', '1', '2016-04-20 01:02:43', '0000-00-00 00:00:00', NULL),
(7, 'Entertain', '', '', '1', '2016-04-20 01:02:48', '0000-00-00 00:00:00', NULL),
(8, 'Sport2', '', '', '1', '2016-04-20 01:25:06', '0000-00-00 00:00:00', NULL),
(9, 'Sport3', '', '', '1', '2016-04-20 13:25:11', '2016-04-20 13:39:48', NULL),
(10, 'Education', '', '', '1', '2016-04-21 14:08:28', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `extention` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_name`, `extention`, `size`, `created_at`, `modified_at`, `deleted_at`) VALUES
(3, '1461391243Capture2.JPG', 'jpg', '111423', '2016-04-20 23:21:35', '2016-04-23 12:00:43', NULL),
(11, '14613990832.JPG', 'jpg', '21185', '2016-04-23 14:11:23', '0000-00-00 00:00:00', NULL),
(12, '14614178653.png', 'png', '7546', '2016-04-23 19:24:25', '0000-00-00 00:00:00', NULL),
(13, '1461418886Capture2.JPG', 'jpg', '111423', '2016-04-23 19:41:26', '0000-00-00 00:00:00', NULL),
(14, '1461419282Capture2.JPG', 'jpg', '111423', '2016-04-23 19:48:02', '0000-00-00 00:00:00', NULL),
(15, '1461420720Mahfuz.JPG', 'jpg', '81766', '2016-04-23 20:12:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `parent_id` varchar(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `parent_id`, `url`, `created_at`, `modified_at`, `deleted_at`) VALUES
(5, 'News', '', '', '2016-04-23 16:04:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Technology', '', '', '2016-04-23 16:15:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Sports', '', '', '2016-04-23 16:16:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Education', '', '', '2016-04-23 16:18:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Programming', '8', '', '2016-04-23 16:32:09', '2016-04-23 18:55:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `personal_phone` varchar(11) DEFAULT NULL,
  `home_phone` varchar(11) DEFAULT NULL,
  `office_phone` varchar(11) DEFAULT NULL,
  `current_address` varchar(255) DEFAULT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `personal_phone`, `home_phone`, `office_phone`, `current_address`, `permanent_address`, `profile_pic`, `created_at`, `modified_at`, `deleted_at`, `modified_by`) VALUES
(4, 12, 'Mahfuzur', 'Rahman', '', '', '', 'Mirpur,Dhaka', 'Savar,Dhaka', '1461086474Mahfuz.JPG', NULL, NULL, NULL, NULL),
(5, 18, 'Rony', 'Khan', '', '', '', '', '', '14610897842014-11-11-024.jpg', NULL, NULL, NULL, NULL),
(6, 19, 'Salahuddin', 'Ahmend', '', '', '345600', '', '', '', NULL, NULL, NULL, NULL),
(7, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_admin` tinyint(4) DEFAULT '1',
  `modified_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_group_id`, `unique_id`, `username`, `password`, `email`, `is_admin`, `modified_at`, `created_at`, `deleted_at`, `is_active`) VALUES
(12, 0, '57164fcf0f953', 'Mahfuz', '123', 'mahfuz@gmail.com', 1, '2016-04-19 21:49:12', '0000-00-00 00:00:00', NULL, 1),
(18, 0, '57165270a0a9c', 'Rony', 'Rony10', 'rony@gmail.com', 0, '2016-04-23 20:50:31', '2016-04-19 21:44:48', NULL, 1),
(19, 0, '5716528fee720', 'salahuddin', 'Sa10', 'salahuddin@yahoo.com', 1, '2016-04-21 15:45:01', '2016-04-19 21:45:19', NULL, 1),
(20, 0, '57165e0da8e78', 'Faria', 'Fa10', 'faria@gmail.com', 1, '2016-04-21 15:45:00', '2016-04-19 22:34:21', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_user_id` (`users_id`);

--
-- Indexes for table `articles_categories_mapping`
--
ALTER TABLE `articles_categories_mapping`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD KEY `fk_PerOrders` (`article_id`), ADD KEY `fk_article_categories` (`category_id`);

--
-- Indexes for table `articles_images_mapping`
--
ALTER TABLE `articles_images_mapping`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_ar_img` (`articles_id`), ADD KEY `fk_im_ar` (`images_id`);

--
-- Indexes for table `articles_menu_mapping`
--
ALTER TABLE `articles_menu_mapping`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_article_menu` (`article_id`), ADD KEY `fk_menu_map` (`menu_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `password` (`password`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `articles_categories_mapping`
--
ALTER TABLE `articles_categories_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `articles_images_mapping`
--
ALTER TABLE `articles_images_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `articles_menu_mapping`
--
ALTER TABLE `articles_menu_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `articles_categories_mapping`
--
ALTER TABLE `articles_categories_mapping`
ADD CONSTRAINT `fk_PerOrders` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
ADD CONSTRAINT `fk_article_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `articles_images_mapping`
--
ALTER TABLE `articles_images_mapping`
ADD CONSTRAINT `fk_ar_img` FOREIGN KEY (`articles_id`) REFERENCES `articles` (`id`),
ADD CONSTRAINT `fk_im_ar` FOREIGN KEY (`images_id`) REFERENCES `images` (`id`);

--
-- Constraints for table `articles_menu_mapping`
--
ALTER TABLE `articles_menu_mapping`
ADD CONSTRAINT `fk_article_menu` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
ADD CONSTRAINT `fk_menu_map` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`);

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
